// MAP INFORMATION
const map = [
    "█████████████████████",
    "█░░░█░░░░░█░░░░░█░█░█",
    "█░█░█░███░█████░█░█░█",
    "█░█░█░░░█░░░░░█░█░░░█",
    "█░███████░█░███░█░█░█",
    "█░░░░░░░░░█░░░░░█░█░█",
    "█░███░█████░█████░█░█",
    "█░█░░░█░░░█░█░░░░░█░█",
    "█░█████░█░█░█░███░█░F",
    "S░░░░░█░█░█░█░█░█░███",
    "█████░█░█░█░█░█░█░█░█",
    "█░░░░░█░█░█░░░█░█░█░█",
    "█░███████░█████░█░█░█",
    "█░░░░░░░█░░░░░░░█░░░█",
    "█████████████████████"
];
// index 20 columns; index 14 rows
const wall = "█";
const walkway = "░";
const start = "S";
const finish = "F";
const mapHere = document.getElementById("mapHere");
const player = document.getElementById("player");

map.forEach((rowString, rowIndex) => {
// for(let rowString of map) {
    let rowDiv = document.createElement("div");
    rowDiv.classList.add("row");
    rowDiv.id = rowIndex
    mapHere.appendChild(rowDiv);
    for(let cellCharacter of rowString) {
        const cellIndex = rowDiv.childElementCount;
        if(cellCharacter === wall) {
            rowDiv.appendChild(createCell("wall", cellIndex));
        } else if(cellCharacter === walkway) {
            rowDiv.appendChild(createCell("walkway", cellIndex));
        } else if(cellCharacter === start) {
            rowDiv.appendChild(createCell("start", cellIndex));
        } else {
            rowDiv.appendChild(createCell("finish", cellIndex));
        }
    }
});
function createCell(type, cellIndex) {
    let cellDiv = document.createElement("div");
    cellDiv.classList.add("cell", type);
    cellDiv.dataset.cell = cellIndex;
    if(type == "start") {
        cellDiv.appendChild(player);
    }
    return cellDiv;
}


// PLAYER INFORMATION
function moveLogicForUpAndDown() {
    const currentCell = player.parentElement;
    const currentRow = currentCell.parentElement;
    let newRowId = Number(currentRow.id) + moveRowIndexBy;
    let newRow = document.getElementById(newRowId);
    let cellCount = currentCell.dataset.cell;
    let destinationCell = newRow.childNodes[cellCount];
    checkForWall(destinationCell) ? null : destinationCell.appendChild(player);
}
function pressDownArrow() {
    moveRowIndexBy = 1;
    moveLogicForUpAndDown();
}
function pressUpArrow() {
    moveRowIndexBy = -1;
    moveLogicForUpAndDown();
}
function pressLeftArrow() {
    let destinationCell = player.parentElement.previousSibling;
    checkForWall(destinationCell) ? null : destinationCell.appendChild(player);
}
function pressRightArrow() {
    let destinationCell = player.parentElement.nextSibling;
    checkForWall(destinationCell) ? null : destinationCell.appendChild(player);
    checkForWin(destinationCell) ? winningMessage() : null;
}
function checkForWall(destinationCell) {
    if(destinationCell.classList.contains("wall")) return true;
}
function checkForWin(destinationCell) {
    if(destinationCell.classList.contains("finish")) return true;
}
function winningMessage() {
    player.style.background = "url(./images/Holly-michael.jpg) no-repeat center center";
    player.style.backgroundSize = "cover";
    const winningMessageHere = document.getElementById("mapHere");
    let messageForWinner = document.createElement("h1");
    messageForWinner.textContent = "YOU ESCAPED TOBY!";
    messageForWinner.classList.add("messageStyle");
    winningMessageHere.appendChild(messageForWinner).style;
    const themeSong = new Audio('./images/themeSong.mp3');
    themeSong.play();
}


document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    switch (keyName) {
        case "ArrowDown":
            pressDownArrow();
            break;
        case "ArrowUp":
            pressUpArrow();
            break;
        case "ArrowLeft":
            pressLeftArrow();
            break;
        case "ArrowRight":
            pressRightArrow();
            break;
    }
});


function resetPlayerToStart() {
    location.reload();
}